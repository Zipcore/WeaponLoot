#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Weapon-Loot"
#define PLUGIN_VERSION "1.1.3"
#define PLUGIN_DESCRIPTION "Spawn weapons as loot"
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <smlib>
#include <cstrike>
#include <multicolors>

#include <zcore/zcore_lootspawner>

char g_sWeapons[][] = {
	"weapon_ak47", "weapon_aug", "weapon_bizon", "weapon_cz75a", "weapon_deagle", "weapon_decoy", "weapon_elite", "weapon_famas", "weapon_fiveseven", "weapon_flashbang", 
	"weapon_g3sg1", "weapon_galilar", "weapon_glock", "weapon_hegrenade", "weapon_hkp2000", "weapon_incgrenade", "weapon_knife", "weapon_m249", "weapon_m4a1", "weapon_awp",
	"weapon_m4a1_silencer", "weapon_mac10", "weapon_mag7", "weapon_molotov", "weapon_mp7", "weapon_mp9", "weapon_negev", "weapon_nova", "weapon_p250", "weapon_p90", "weapon_revolver", 
	"weapon_sawedoff", "weapon_scar20", "weapon_sg556", "weapon_smokegrenade", "weapon_ssg08", "weapon_taser", "weapon_tec9", "weapon_ump45", "weapon_usp_silencer", "weapon_xm1014",
	"weapon_healthshot", "weapon_tagrenade"
};

#define MAXWEAPONS sizeof(g_sWeapons)

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("ttt-weapons");
	
	return APLRes_Success;
}

enum WP_Loot
{
	Loot_Type, // The typeID we get from ZCore-LootSpawner
	String:Loot_Name[32],
	String:Loot_Model[256],
	
	Loot_MaxSpawned,
	Loot_Weight,
	Loot_RoundStartWeight,
	
	bool:Loot_Use,
	bool:Loot_Touch,
	
	String:Loot_Weapon[64]
};

int g_eLoot[128][WP_Loot];
int g_iLootGroupCount;

/* Plugin Start */

public void OnPluginStart()
{
	CreateConVar("ttt_weapon_loot_version", PLUGIN_VERSION, "Weapon-Loot version", FCVAR_DONTRECORD|FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
}

public void OnMapStart()
{
	// Load loot
	Handle kvConfig = CreateKeyValues("Weapon-Loot");
	
	char sPath[256];
	BuildPath(Path_SM, sPath, 255, "configs/weapon-loot.txt");
	FileToKeyValues(kvConfig, sPath);
	
	if(!KvGotoFirstSubKey(kvConfig))
		return;
	
	g_iLootGroupCount = 0;
	do
	{
		KvGetSectionName(kvConfig, g_eLoot[g_iLootGroupCount][Loot_Name], 32);
		
		KvGetString(kvConfig, "model", g_eLoot[g_iLootGroupCount][Loot_Model], 256, "");
		
		g_eLoot[g_iLootGroupCount][Loot_Use] = view_as<bool>(KvGetNum(kvConfig, "use", 0));
		g_eLoot[g_iLootGroupCount][Loot_Touch] = view_as<bool>(KvGetNum(kvConfig, "touch", 1));
		
		int iFlags;
		
		if(g_eLoot[g_iLootGroupCount][Loot_Use])
			iFlags |= LS_FLAG_USE;
			
		if(g_eLoot[g_iLootGroupCount][Loot_Touch])
			iFlags |= LS_FLAG_TOUCH;
		
		g_eLoot[g_iLootGroupCount][Loot_Weight] = KvGetNum(kvConfig, "weight", 0);
		g_eLoot[g_iLootGroupCount][Loot_RoundStartWeight] = KvGetNum(kvConfig, "round_start_weight", 0);
		g_eLoot[g_iLootGroupCount][Loot_MaxSpawned] = KvGetNum(kvConfig, "max_spawned", 100);
		
		KvGetString(kvConfig, "weapon", g_eLoot[g_iLootGroupCount][Loot_Weapon], 64, "");
		
		// Register loot
		char sName[32];
		strcopy(sName, 32, g_eLoot[g_iLootGroupCount][Loot_Name]);
		
		char sModel[256];
		strcopy(sModel, 256, g_eLoot[g_iLootGroupCount][Loot_Model]);
		
		g_eLoot[g_iLootGroupCount][Loot_Type] = ZCore_LootSpawner_RegisterLootType(sName, sModel, 0, 0, g_eLoot[g_iLootGroupCount][Loot_Weight], g_eLoot[g_iLootGroupCount][Loot_RoundStartWeight], g_eLoot[g_iLootGroupCount][Loot_MaxSpawned], iFlags, 0, 0);
		
		g_iLootGroupCount++;
		
	} while (KvGotoNextKey(kvConfig));
	
	CloseHandle(kvConfig);
}

int GetLootIndex(int type)
{
	for (int i = 0; i < g_iLootGroupCount; i++)
	{
		if(g_eLoot[i][Loot_Type] != type)
			continue;
			
		return i;
	}
	
	return -1;
}

public Action ZCore_LootSpawner_OnLootAction(int client, int entity, int type, int action)
{
	int index = GetLootIndex(type);
	
	// Is this a lootgroup we have registered?
	if(index == -1)
		return Plugin_Continue;
	
	if(action == LS_ACTION_STARTTOUCH)
	{
		char sWeapon[64];
		Format(sWeapon, sizeof(sWeapon), "%s", g_eLoot[index][Loot_Weapon]);
		
		for(int i = 0; i < MAXWEAPONS; i++)
		{
			if(StrContains(g_sWeapons[i], sWeapon) == -1)
				continue;
			
			GivePlayerItem(client, g_sWeapons[i]);
			CPrintToChat(client, "You have found an untouched %s.", g_eLoot[index][Loot_Name]);
			
			break;
		}
		
		ZCore_LootSpawner_RemoveLoot(entity, client, LS_ACTION_REMOVE);
	}
	
	return Plugin_Continue; //Remove the entity
}