This plugin allows you do add random weapon spawns on maps. 

**Install Instructions:**
- Upload this plugin and all required plugins to your server
- Edit "configs/weapon-loot.txt" to adjust spawn chances
- Follow [instructions for Z-Core-LootSpawner](https://gitlab.com/Zipcore/ZCore-LootSpawner/blob/master/README.md) to create loot spawns on your maps

**Config:**
- [Loot Table](http://gitlab.com/Zipcore/WeaponLoot/blob/master/configs/weapon-loot.txt)

**Required plugins:**
- [Z-Core-LootSpawner](http://gitlab.com/Zipcore/ZCore-LootSpawner) ([download](https://gitlab.com/Zipcore/ZCore-LootSpawner/repository/archive.zip?ref=master))